export default {
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASS,
  },
  secure: false,
  default: {
    from: 'Equipe Gobarber <noreply@gobarber.com>',
  },
};

//Amazon SES
//MAILGUN
//SPARKPOST
//MANDRIL (MAILCHIMP)

//utilizar o mailtrap para desenv.

//utilizar o handlebars para criacao de templates com variaveis do node.
